const express = require("express");
const User = require("../models/User");
const bcrypt = require("bcryptjs");

const router = express.Router();

router.delete("/delete", async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email }).select("+password");

  if (!user) {
    return res.status(400).send({ error: "User not found" });
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).send({ error: "nvalid password" });
  }

  try {
    await User.findOneAndDelete({ email });
  } catch (err) {
    console.log("error", err);
    return res
      .status(400)
      .send({ error: "The user can't be deleted", message: [err] });
  }

  user.password = undefined;

  return res
    .status(200)
    .send({ message: "The user " + user.email + " has deleted successfully" });
});

router.post("/register", async (req, res) => {
  const { email } = req.body;

  try {
    if (await User.findOne({ email })) {
      return res.status(400).send({ error: "User already exists." });
    }

    const user = await User.create(req.body);

    user.password = undefined;

    return res.send({ user });
  } catch (err) {
    return res.status(400).send({ error: "Registration failed" });
  }
});

module.exports = app => {
  app.use("/user", router);
};
