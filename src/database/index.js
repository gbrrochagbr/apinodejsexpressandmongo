const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost/noderest", {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true
});
//mongoose.set("useFindAndModify", false);

mongoose.Promise = global.Promise;

module.exports = mongoose;
