const path = require("path");
const nodemailer = require("nodemailer");
const hbs = require("nodemailer-express-handlebars");
const mailConfig = require("../config/mail.json");

const transport = nodemailer.createTransport(mailConfig);

transport.use(
  "compile",
  hbs({
    viewEngine: {
      extName: ".hbs",
      partialsDir: path.resolve("./src/resources/mail")
    },
    viewPath: path.resolve("./src/resources/mail"),
    extName: ".hbs"
  })
);

module.exports = transport;
